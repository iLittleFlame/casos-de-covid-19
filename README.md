# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

En el directorio del proyecto, tu puedes correr:

### `npm start`

Corre la App en el modo de desarrollo.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Nota.\
Se pueden ver algunos errores en consola es normal.

### `npm test`

Ejecuta el modo pruebas para mirar el mode de interaccion.\

### `npm run build`




